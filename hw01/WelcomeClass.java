//////////////
//// CSE 02 WelcomeClass
//// Hw01
///  Conor Leahy 
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints a Welcome message and digits of lehigh network ID
    System.out.println("    -----------");
		System.out.println("    | WELCOME |");
		System.out.println("    -----------");
		System.out.println("  ^  ^  ^  ^  ^  ^ ");
		System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
		System.out.println("<-8--1--0--8--3--4->");
		System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /  ");
		System.out.println("  v  v  v  v  v  v  ");
		
		//use multiple print statements to get unique welcome message
		
  }

}
