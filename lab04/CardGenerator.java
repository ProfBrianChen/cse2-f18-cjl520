////Conor Leahy     CSE 002
////Card Generator   Lab04
////Program will generate a random card from a deck of cards and output result to user


public class CardGenerator{
	//main method required for every java program
	public static void main(String args[]){
		
		int randGen = (int)(Math.random()*54)+1; //Generate a randome number from 1-54
		int cardNumber; //initialized integer value for card number
		String suitName = "";  // Set up strings for card suit and identity
		String identity = "";
		
		if(randGen<=13){
		suitName = "diamonds";
		} 
		/// if card number is less than or equal to 13 it is a diamond 
			
		else if(randGen<=26){
			suitName = "clubs";
			randGen -=13;
		}
			//If the number is also less than or equal to 26 then clubs is generated and you 
			///subtract 13 in order to get integer bewteen 1 and 13 for identity
		
		else if(randGen<=39){
			suitName = "hearts";
			randGen -=26;
		}
			//If the number is also less than or equal to 39 then hearts is generated and you 
			///subtract 26 in order to get integer bewteen 1 and 13 for identity
		
		else if(randGen<=52){
			suitName = "spades";
			randGen -=39;
		}
			//If the number is also less than or equal to 39 then spades is generated and you 
			///subtract 39 in order to get integer bewteen 1 and 13 for identity
			
		else{
		System.out.println("Error with random generator");
	}
			//outputs error if program can't assign suit
		
			switch(randGen){
				case 1 : identity = "Ace";
				break;
				case 2 : identity = "Two";
				break;
				case 3 : identity = "Three";
				break;
				case 4 : identity = "Four";
				break;
				case 5 : identity = "Five";
				break;
				case 6 : identity = "Six";
				break;
				case 7 : identity = "Seven";
				break;
				case 8 : identity = "Eight";
				break;
				case 9 : identity = "Nine";
				break;
				case 10 : identity = "Ten";
				break;
				case 11 : identity = "Jack";
				break;
				case 12 : identity = "Queen";
				break;
				case 13 : identity = "King";
				break;
				//using switch to assign identity to card based off integer value in between 1 and 13 of randGen after you generate it and adjust 
			}
				System.out.println("You picked the " + identity + " of " + suitName);
	}
	
}