///Conor Leahy CSE 002
///HW05 


///Your program should ask the user how many times it should generate hands. 
//For each hand, your program should generate five random numbers from 1 to 52, 
//where these 52 cards represent the four sets of 13 cards associated with each suit.  
//Specifically, cards 1-13 represent diamonds; 14-26 represent clubs; then hearts; then spades.  
//In all suits, card identities ascend in step with the card number.  
//For example, 14 is the ace of clubs; 15 is the 2 of clubs; and 26 is the king of clubs.  
//Then, your program should check if this set of five cards belongs to one of the four hands listed in the above. 
//If so, the counter for this hand should be incremented by one. 
//After all hands have been generated, your program should calculate the probabilities of each of 
//the four hands described above by computing the number of occurrences divided by the total number of hands.
///Note: You must also guard against duplicate cards generated in one hand.  For example, 
///if the first card drawn is 14 (= ace of clubs), you must ensure that this card is not drawn again for 
//the four remaining cards in that hand.

import java.util.*;
//import all objects including scanner and random

public class Hw05 {
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in); ///name instance of scanner
	  Random randGen = new Random();  ///name instace of random generator 
		boolean input =  false; ///conditional for use in while loop of initial input of integer
		
	
		
		double numHands = 0;  ///initialize basic variables of number of hands generated and cards within 
		int rand1 = 0;  ///random numbers attributed to each card (1-52)
		int rand2 = 0;
		int rand3 = 0;
		int rand4 = 0;
		int rand5 = 0;
		int rand = 0;   ////NOT SURE
		
		int card1 = 0;   ///variables to convert to number between 1-13 to ignore suit
		int card2 = 0; 
		int card3 = 0;
		int card4 = 0;
		int card5 = 0;
		
		int numAce = 0; ///convert to card variables to these to determine number of each type of card in hand
		int num2 = 0;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		int num7 = 0;
		int num8 = 0;
		int num9 = 0;
		int num10 = 0;
		int numQueen = 0;
		int numJack = 0;
		int numKing = 0;
		
		int numPairsPerLoop = 0;
		
		
		double numPairs = 0;  ///variables that will be attributable to value of hand given by if statements
		double numTwoPair = 0;
		double numThreeKind = 0;
		double numFourKind = 0;
		double numOnePair = 0; ///used to help differentiate between one pair and two pair
		
		double probOnePair = 0; ///variables that will be attributable with probability of hand type
		double probTwoPair = 0;
		double probThreeKind = 0;
		double probFourKind = 0;
		
		
		System.out.println("Enter the number of hands you want generated: ");
		input = myScanner.hasNextInt();
		while (input == false) {
// 			if (myScanner.hasNextInt()) {
			  myScanner.next(); 
				input = myScanner.hasNextInt();
// 				numHands = myScanner.nextInt(); 
// 				input = true;
				
// 			}
// 			else {
// 				myScanner.next();  ///clear out previous erroneous input
// 				System.out.println ("ERROR, please enter integer value: "); ///loop restarts and scanner rechecks user input
// 		    System.out.println ("Enter the course number for course: ");
// 			}
		}
		numHands = myScanner.nextInt();
		
		for (int i = 0; i < numHands ; ++i) { 
			///Set up for loop to generate unique hands = to user input and stop after this number is met by deincrementing by 1 after each loop
			numAce = 0; ///redefine these variables as zero each time so counter is only
			num2 = 0;    ///incremented with each seperate hand and not continual aggregation of all hands
			num3 = 0;
			num4 = 0;
		  num5 = 0;
		  num6 = 0;
		  num7 = 0;
		  num8 = 0;
		  num9 = 0;
		  num10 = 0;
		  numJack = 0;
			numQueen = 0;
		  numKing = 0;
			
		
			rand1 = randGen.nextInt(52) +1;    ///Generate the random number for first card of hand
			rand2 = randGen.nextInt(52) +1;     ////second card
			  while (rand2 == rand1) {    ///while loop ensures numbers are unique and process repeated in order to get unique hand
					rand2 = randGen.nextInt(52) +1;
				}
			rand3 = randGen.nextInt(52) +1;
			   while (rand3 == rand1 || rand3 == rand2 ) {
					 rand3 = randGen.nextInt(52) +1;
					 }
			rand4 = randGen.nextInt(52) +1;
			   while (rand4 == rand3 || rand4 == rand2 || rand4 == rand1) {
					 rand4 = randGen.nextInt(52) +1;
				 }
			rand5 = randGen.nextInt(52) +1;
			   while (rand5 == rand4 || rand5 == rand3 || rand5 == rand2 || rand5 == rand1) {
					 rand5 = randGen.nextInt(52) +1;
				 }
			
			card1 = rand1 % 13; ///by taking remainder after divided by 13 you ignore suit and organize card by value alone
			card2 = rand2 % 13;  ///don't need to know suit for values of hand asked for in program
			card3 = rand3 % 13;
			card4 = rand4 % 13;
			card5 = rand5 % 13;
			
			switch (card1) {  ///Evaulates value of card 1 based on number from 1-13 and adds that number to counter for that value
				case 0: numAce++;
					break;
				case 1: num2++;
					break;
				case 2: num3++;
					break;
				case 3: num4++;
					break;
				case 4: num5++;
					break;
				case 5: num6++;
					break;
				case 6: num7++;
					break;
				case 7: num8++;
					break;
				case 8: num9++;
					break;
				case 9: num10++;
					break;
				case 10: numJack++;
					break;
				case 11: numQueen++;
					break;
				case 12: numKing++;
					break;
				}
			  switch (card2) {  ///Evaulates value of card 2 based on number from 1-13 and adds that number to counter for that value
				case 0: numAce++;
					break;
				case 1: num2++;
					break;
				case 2: num3++;
					break;
				case 3: num4++;
					break;
				case 4: num5++;
					break;
				case 5: num6++;
					break;
				case 6: num7++;
					break;
				case 7: num8++;
					break;
				case 8: num9++;
					break;
				case 9: num10++;
					break;
				case 10: numJack++;
					break;
				case 11: numQueen++;
					break;
				case 12: numKing++;
					break;
				}
			  switch (card3) {  ///Evaulates value of card 3 based on number from 1-13 and adds that number to counter for that value
				case 0: numAce++;
					break;
				case 1: num2++;
					break;
				case 2: num3++;
					break;
				case 3: num4++;
					break;
				case 4: num5++;
					break;
				case 5: num6++;
					break;
				case 6: num7++;
					break;
				case 7: num8++;
					break;
				case 8: num9++;
					break;
				case 9: num10++;
					break;
				case 10: numJack++;
					break;
				case 11: numQueen++;
					break;
				case 12: numKing++;
					break;
				}
			  switch (card4) {  ///Evaulates value of card 4 based on number from 1-13 and adds that number to counter for that value
	      case 0: numAce++;
					break;
				case 1: num2++;
					break;
				case 2: num3++;
					break;
				case 3: num4++;
					break;
				case 4: num5++;
					break;
				case 5: num6++;
					break;
				case 6: num7++;
					break;
				case 7: num8++;
					break;
				case 8: num9++;
					break;
				case 9: num10++;
					break;
				case 10: numJack++;
					break;
				case 11: numQueen++;
					break;
				case 12: numKing++;
					break;
				}
			  switch (card5) {  ///Evaulates value of card 5 based on number from 1-13 and adds that number to counter for that value
				case 0: numAce++;
					break;
				case 1: num2++;
					break;
				case 2: num3++;
					break;
				case 3: num4++;
					break;
				case 4: num5++;
					break;
				case 5: num6++;
					break;
				case 6: num7++;
					break;
				case 7: num8++;
					break;
				case 8: num9++;
					break;
				case 9: num10++;
					break;
				case 10: numJack++;
					break;
				case 11: numQueen++;
					break;
				case 12: numKing++;
					break;
				}
				
			  
			  if (numAce == 2) {  ///Checks to see if exactly two values are equal in a single hand 
					                 //and increments the counter for pairs accordingly
					numPairs++;
				}
			  if (num2 == 2) {
					numPairs++;
				}
			  if (num3 == 2) {
					numPairs++;
				}
			  if (num4 == 2) {
					numPairs++;
				}
			  if (num5 == 2) {
					numPairs++;
				}
			  if (num6 == 2) {
					numPairs++;
				}
			  if (num7 == 2) {
					numPairs++;
				}
			  if (num8 == 2) {
					numPairs++;
				}
			  if (num9 == 2) {
					numPairs++;
				}
			  if (num10 == 2) {
					numPairs++;
				}
			  if (numJack == 2) {
					numPairs++;
				}
			  if (numQueen == 2) {
					numPairs++;
				}
			  if (numKing == 2) {
					numPairs++;
				}
			  
			  if (numAce == 3) {  ///Checks to see if exactly three values are equal in a single hand 
					                 //and increments the counter for three of kind accordingly
					numThreeKind++;
				}
			  if (num2 == 3) {
					numThreeKind++;
				}
			  if (num3 == 3) {
					numThreeKind++;
				}
			  if (num4 == 3) {
					numThreeKind++;
				}
			  if (num5 == 3) {
					numThreeKind++;
				}
			  if (num6 == 3) {
					numThreeKind++;
				}
			  if (num7 == 3) {
					numThreeKind++;
				}
			  if (num8 == 3) {
					numThreeKind++;
				}
			  if (num9 == 3) {
					numThreeKind++;
				}
			  if (num10 == 3) {
					numThreeKind++;
				}
			  if (numJack == 3) {
					numThreeKind++;
				}
			  if (numQueen == 3) {
					numThreeKind++;
				}
			  if (numKing == 3) {
					numThreeKind++;
				}
			  
			
			  if (numAce == 4) {  ///Checks to see if exactly four values are equal in a single hand 
					                 //and increments the counter for four of a kind accordingly
					numFourKind++;
				}
			  if (num2 == 4) {
					numFourKind++;
				}
			  if (num3 == 4) {
					numFourKind++;
				}
			  if (num4 == 4) {
					numFourKind++;
				}
			  if (num5 == 4) {
					numFourKind++;
				}
			  if (num6 == 4) {
					numFourKind++;
				}
			  if (num7 == 4) {
					numFourKind++;
				}
			  if (num8 == 4) {
					numFourKind++;
				}
			  if (num9 == 4) {
					numFourKind++;
				}
			  if (num10 == 4) {
					numFourKind++;
				}
			  if (numJack == 4) {
					numFourKind++;
				}
			  if (numQueen == 4) {
					numFourKind++;
				}
			  if (numKing == 4) {
					numFourKind++;
				}
			  
			numPairsPerLoop = numPairsPerLoop + (int) numPairs;
			
			  if (numPairs == 2) {  ///When two pairs exist in a hand it will subtract value from pairs counter and add one to two pair counter
					numTwoPair++;
					numPairs -=1;  //avoid double counting pairs 
				}
			  
			numPairs = 0;
			

			} /// end of for loop (HUUUUUUUUGE)
		    
		 probOnePair = (double) (numPairsPerLoop)/(double) (numHands);  //calculate probabilies of each by utilzing counters overs all hands divided by number of hands for each type of hand
		 probTwoPair = (double) (numTwoPair)/(double) (numHands);
		 probThreeKind = (double) (numThreeKind)/(double)(numHands);
		 probFourKind = (double) (numFourKind)/(double)(numHands);
		
		/*if ((probOnePair * 10000) % 10 >= 5) {  ///Talked with classmate and TA assistance on this one
			probOnePair = probOnePair + .0005;    ////Trying to ensure probability not rounded down to zero each time
		}
		int converter = (int) (probOnePair * 1000);    ///ensures that some decimals are considered reformulating them and casting as double
		probOnePair = (double) converter * .001;
			
		
		if ((probTwoPair * 10000) % 10 >= 5) {  ///Talked with classmate and TA assistance on this one
			probTwoPair = probTwoPair + .0005;    ////Trying to ensure probability not rounded down to zero each time
		}
	  converter = (int) (probTwoPair * 1000);    ///ensures that some decimals are considered reformulating them and casting as double
		probTwoPair = (double) converter * .001;
		  
	
	  if ((probThreeKind * 10000) % 10 >= 5) {  ///Talked with classmate and TA assistance on this one
			probThreeKind = probThreeKind + .0005;    ////Trying to ensure probability not rounded down to zero each time
		}
		converter = (int) (probThreeKind * 1000);    ///ensures that some decimals are considered reformulating them and casting as double
		probThreeKind = (double) converter * .001;
	
		
		if ((probFourKind * 10000) % 10 >= 5) {  ///Talked with classmate and TA assistance on this one
			probFourKind = probFourKind + .0005;    ////Trying to ensure probability not rounded down to zero each time
		}
		converter = (int) (probFourKind * 1000);    ///ensures that some decimals are considered reformulating them and casting as double
		probFourKind = (double) converter * .001;
		
		
		double probOnePair = ((double) numPairs ) / ((double) numHands);
		double probTwoPair = ((double) numTwoPair) / ((double) numHands);
		double probThreeKind = ((double) numThreeKind) / ((double) numHands);
		double probFourKind = ((double) numFourKind) / ((double) numHands);
		*/
		
		System.out.println("Number of hands: " + numHands);
		System.out.printf("Probability of a single pair is: %.3f\n", probOnePair);
		System.out.printf("Probability of a two pair is: %.3f\n",  probTwoPair);
		System.out.printf("Probability of a three of a kind is: %.3f\n",  probThreeKind);
		System.out.printf("Probability of a four of a kind is: %.3f\n",  probFourKind);
			
			
		////Here is where I cross my fingers and hope it compiles
		
		
	}
	} ///end of code 
