//// hw02     CSE 002
//// Conor Leahy
//// Arithmetic
//
public class Arithmetic{
	public static void main(String args[]){
		///
		
		
		//Number of pairs of pants
		int numPants = 3;
		//cost per pair of pants
		double pantsPrice = 34.98;
		
		//Number of sweatshirts
		int numShirts = 2;
		//Cost per shirt
		double shirtPrice = 24.99;
		
		//Number of belts
		int numBelts = 1;
		//cost per belts
		double beltCost = 33.99;
		
		//the tax rate
		double paSalesTax = 0.06;
		
		double totalCostofPants;    //total cost of pants 
		double totalCostofShirts;    //total cost of shirts
		double totalCostofBelts;    //total cost of belts
		
		double salesTaxPants;    //sales tax charged on pants
		double salesTaxShirts;    //sales tax charged on shirts
		double salesTaxBelts;   //sales tax charged on belts
		
		double totalCostPurchases;    //total cost of purchases before tax
		double totalSalesTax;     //total sales tax on all items
		double totalPaid;    //total paid including sales tax
		
		totalCostofPants = numPants * pantsPrice;     //Compute total cost for each items
		totalCostofShirts = numShirts * shirtPrice;
		totalCostofBelts = numBelts * beltCost;
		
		System.out.println("The total cost of pants is: " + totalCostofPants);
		System.out.println("The total cost of shirts is: " + totalCostofShirts);
		System.out.println("The total cost of belts is: " + totalCostofBelts);
		
		salesTaxPants = totalCostofPants * paSalesTax;    //Compute and print the sales tax paid for each type of item 
		salesTaxPants = salesTaxPants * 100;      //round sales tax values so on to two decimal places
		salesTaxPants = Math.round(salesTaxPants);
		salesTaxPants = salesTaxPants / 100;
		
		salesTaxShirts = totalCostofShirts * paSalesTax;
		salesTaxShirts = salesTaxShirts * 100;      //round sales tax values so up to two decimal places
		salesTaxShirts = Math.round(salesTaxShirts);
		salesTaxShirts = salesTaxShirts / 100;
		
		salesTaxBelts = totalCostofBelts * paSalesTax;
		salesTaxBelts = salesTaxBelts * 100;      //round sales tax values so on to two decimal places
		salesTaxBelts = Math.round(salesTaxBelts);
		salesTaxBelts = salesTaxBelts / 100;
		
		System.out.println("The sales tax on pants is: " + salesTaxPants);
		System.out.println("The sales tax on shirts is: " + salesTaxShirts);
		System.out.println("The sales tax on belts is: " + salesTaxBelts);
		
		totalCostPurchases = totalCostofPants + totalCostofShirts + totalCostofBelts;   
		///Computing and displaying the total cost of purchases
		
		System.out.println("The total cost of purchases before tax is: " + totalCostPurchases);
		
		totalSalesTax = totalCostPurchases * paSalesTax;
		// Computing and displaying the total sales tax on all purchases
		totalSalesTax = totalSalesTax * 100;      //round sales tax values so on to two decimal places
		totalSalesTax = Math.round(totalSalesTax);
		totalSalesTax = totalSalesTax / 100;
		
		System.out.println("The total sales tax is: " + totalSalesTax);
		
		totalPaid = totalSalesTax + totalCostPurchases;
		//computing and displaying the total amount paid after sales tax
		totalPaid = totalPaid * 100;      //round sales tax values so on to two decimal places
		totalPaid = Math.round(totalPaid);
		totalPaid = totalPaid / 100;
		
		System.out.println("The total amount paid after tax is: " + totalPaid);
		
		
		
		
		
		
	}
	
}