//// Conor Leahy    
//// 9/6/18
////  CSE02
//// Program runs to print information related to the time elapsed in seconds and number of rotations of wheel for a bike ride
//
public class Cyclometer {
	//
	public static void main(String [] args){
		

int secsTrip1=480;  //seconds in trip one
int secsTrip2=3220;  //seconds in trip two
int countsTrip1=1561;  //wheel rotations in trip 1561
int countsTrip2=9037;  //wheel rotations in trip 2

double wheelDiameter=27.0,   //diameter of the wheels
PI=3.14159,  //Value of pi for calculations of circles
feetPerMile=5280,  //conversion rate for feet per mile
inchesPerFoot=12,  //conversion rate for inches per foot
secondsPerMinute=60;  //conversion rate for seconds per minute
double distanceTrip1, distanceTrip2, totalDistance;  //Iniatilized the decible value for distance of trip 1, 2, and their sum

System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute) +" minutes and had "+countsTrip1+" counts.");
System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute) +"minutes and had "+countsTrip2+" counts.");

distanceTrip1=countsTrip1*wheelDiameter*PI;
//Above gives distance of trip 1 in inches
//For each count, a rotation of the wheel travels the diameter in inches times PI
distanceTrip1/=inchesPerFoot*feetPerMile; //Gives distance in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Distance in miles of trip 2
totalDistance=distanceTrip1+distanceTrip2;

//print the output data
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was " + totalDistance+" miles");
		
			}
}