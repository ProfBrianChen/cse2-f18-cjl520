///Conor Leahy
///CSE002       9/13/18
////lab 03      Check.java
/////Uses scanner to obtain from user the origiincal cost of check to calculate tip, and number of was check will be split

import java.util.Scanner;

public class Check{
	//main method required for every Java Program
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		//Constuct instance of scanner to ask it for input from user
		
		System.out.print("Enter the original cost of the check in the form xx.xx: ");
		//Prompt user for original cost of bill
		double checkCost = myScanner.nextDouble();
		//scans users input to store as double cost of check
		System.out.print("Enter the percentage tip that you wush to pay as a whole number (in the form xx): ");
		//Prompts use for tip they wish to pay
		double tipPercent = myScanner.nextDouble();
		//stores tip percentage value
		tipPercent /=100; //We want to convert the percentage into a decimal value
		System.out.print("Enter the number of people who went out to dinner: ");
		int numPeople = myScanner.nextInt();
		//Prompts user for integer value of number of people out at dinner and scanner records
		
		double totalCost;
		double costPerPerson;
		int dollars, //whole dollar amount of cost
			dimes, pennies;  //for storing digits
				//to the right of the decimal point
				//for the cost$
		totalCost = checkCost * (1 + tipPercent);
		costPerPerson = totalCost / numPeople;
		//get the whole amount, dropping the decimal fraction 
		dollars=(int)costPerPerson;
		//get dimes amount, e.g,
		//int(6.73 *10) % 10 turns into 67 % 10 which equals 7
		//Where the % (mod) operator returns the remainder
		
		dimes = (int)(costPerPerson * 10) %10;
		pennies = (int)(costPerPerson *100) %10;
		System.out.println("Each person in the group oews $" + dollars + '.' + dimes + pennies);
		
		
		
		
	}//end of main method
	
}//end of class