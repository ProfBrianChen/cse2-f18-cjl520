////Conor Leahy
/////CSE 002     HW03   Convert.java
/////Computes how many inches of rain were dropped on average by a 
///hurricane after given the number of acres of land affected by it

import java.util.Scanner;

public class Convert{
	///main method required by every jave program
	public static void main(String [] args){
		
		
		Scanner MyScanner = new Scanner(System.in);
		//Conduct instance of scanner to ask it for input from the user
		
		System.out.println("EMERGENCY, there is a hurricane in your area. Please enter the following info:");
		
		System.out.println("Enter acres affected by hurricane: ");
		double numAcres = MyScanner.nextDouble();
		///scans user input in order to get acres of land affected by hurricane
		
		System.out.println("How many inches of rain were dropped on average: ");
		double numRain = MyScanner.nextDouble();
///Asks and scans user input for avrage inches of rain
		
		double RainGallons = numAcres * (numRain * 27154); ///Set up conversion to amount of rain in gallons
		double RainCubicMiles = RainGallons * 9.0817e-13;  ///set up conversion to rain in cubic miles
		
		System.out.println("The amount of precipitation in the area is " + RainCubicMiles + " cubic miles");
		///Outputs rain in cubic miles
		
			
		
												 
												 }
	
}
