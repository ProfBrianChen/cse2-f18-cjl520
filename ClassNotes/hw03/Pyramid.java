///Conor Leahy    CSE 002
///// Pyramid.java     HW 03
///Takes user input for the dimensions of the pyramid and returns volume inside pyramid 

import java.util.Scanner;

public class Pyramid{
	///main method required by every jave program
	public static void main(String [] args){
		
		
		Scanner MyScanner = new Scanner(System.in);
		////Conduct instance of scanner to ask it for input from the user
		
		System.out.println("The square side of the pyramid (input length): ");
		double numSide = MyScanner.nextDouble();
		///prompt user for and scan for side length of pyramid
		
		System.out.println ("The height of the pyramid (input height): ");
		double numHeight = MyScanner.nextDouble();
		///prompt user for and scan value for height of pyramid 
		
		double numVolume = ((numSide * numSide) * numHeight )/3; 
		///calculate and initialize the volume
		
		System.out.println("The volume inside the pyramid is " + numVolume);
		///Output volume of pyramid
	}
	
}