/// CSE002 Conor Leahy lab06 part 1

import java.util.*;

public class patternB {
	public static void main (String [] args ) {
		
	Scanner Scan = new Scanner (System.in);
	int input = 0;
	boolean intcheck = false;
	String temp = "";
	int count = 1;
	int numRows = 0;
		
	
	System.out.println("Enter an integer from 1-10: ");
	while (intcheck == false) { ///makes sure the number entered is an integer from 1 to 10
		if (Scan.hasNextInt()) { //boolean test for whether input is an integer 
		     input = Scan.nextInt();
		  if (input <= 10 && input > 0) {
			  intcheck = true; //ends loop if both conditions of integer and value from 1-10 are met
			 
		  }
		  else {
			  System.out.println("Error: Please enter integer from 1-10: ");
		  }
		}
	}
	//Pattern B
	
		
	for (numRows = input; numRows > 0 ; numRows--); {
		while (count <= numRows) {
			System.out.print(count +" ");
			count++;
		}
		System.out.println("");
		count = 1;
	}
	}
}