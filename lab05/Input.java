import java.util.*;
//imports all objects including scanner

public class Input {
//initilize class and main method necessary in all code
   public static void main(String[] args){
		 Scanner myScanner = new Scanner (System.in);  ///declare instance of scanner
		 
///set up conditionals for while loops
boolean courseNum = false;
boolean deptName = false;
boolean weeklyMeets = false;
boolean classTime = false;
boolean teacherName = false;
boolean numStudents = false;

///user input values
int course = 0;
String dept = "";
int weekly = 0; 
int time = 0;
String teacher = "";
int students = 0;
		 
System.out.println ("Enter the course number for course: ");
while (courseNum == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNextInt()) {//outputs true or false if user input is an integer
		course = myScanner.nextInt(); //sets course equal to user input
	  courseNum = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter integer value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter the course number for course: ");
	}
}
	
		 
	System.out.println ("Enter the department name for course: ");
  while (deptName == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNext() && !myScanner.hasNextInt() && !myScanner.hasNextDouble()) {//outputs true or false if user input is an integer
		dept = myScanner.next(); //sets course equal to user input
	  deptName = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter  string value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter the department name for course: ");
	}
}
		 
		 
		 System.out.println ("Enter number of times course meets weekly: ");
while (weeklyMeets == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNextInt()) {//outputs true or false if user input is an integer
		weekly = myScanner.nextInt(); //sets course equal to user input
	  weeklyMeets = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter integer value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter number of times course meets weekly: ");
	}
}
		 
  System.out.println ("Enter the class time for course: ");
while (classTime == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNextInt()) {//outputs true or false if user input is an integer
		time = myScanner.nextInt(); //sets course equal to user input
	  classTime = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter integer value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter the class time for course: ");
	}
} 
		
		
	System.out.println ("Enter the teacher name for course: ");
  while (teacherName == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNext() && !myScanner.hasNextInt() && !myScanner.hasNextDouble()) {//outputs true or false if user input is an integer
		teacher = myScanner.next(); //sets course equal to user input
	  teacherName = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter  string value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter the teacher name for course: ");
	}
}
		 
	 System.out.println ("Enter the number of students for course: ");
while (numStudents == false) {   ///use conditional to see if user applies correct value
	if (myScanner.hasNextInt()) {//outputs true or false if user input is an integer
		students = myScanner.nextInt(); //sets course equal to user input
	  numStudents = true; ///changes conditional to exit while loop
	}
	else {
		myScanner.next(); ///clears user input
		System.out.println ("ERROR, please enter integer value: "); ///loop restarts and scanner rechecks user input
		System.out.println ("Enter the number of students for course: ");
	}
} 
		 
		 System.out.println("The course number is: " + course);
		  System.out.println("The department name is: " + dept);
		  System.out.println("The number of times the course meets weekly is: " + weekly);
		  System.out.println("The teacher name is: " + teacher);
		  System.out.println("The number of students is: " + students);
		 
		 
	 
	 
	 }
}