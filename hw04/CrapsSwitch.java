///Conor Leahy CSE 002 
////HW 04 CrapsSwitch.java
////evaluate common craps slang on die roles using switch statements 

import java.util.*;

public class CrapsSwitch {
	///main method required by every java program
	public static void main (String [] args) {
	Scanner myScanner = new Scanner (System.in); 
	///declate scanner
	Random myRandom = new Random(); ///set up random variable
	String dieRoll = ""; ///declare necessary variables
	int dice1 = 0;
	int dice2 = 0;
	
	System.out.println ("Type 1 to randomly cast die and 2 to state die values to evaluate:");
	int choice1 = myScanner.nextInt(); /// stores value of choice 1 from user input
	
	switch (choice1 ) {
		case 1:
			dice1 = myRandom.nextInt(6) + 1; ///create random number between 1 and 6 
			dice2 = myRandom.nextInt(6) + 1; 
			break;
		case 2: 
			System.out.println ("Please choose a value for the first die"); ///ask user for first die value between 1-6
			dice1 = myScanner.nextInt();
			switch ( dice1 ) {
				case 1:
					dice1 = 1;
					break;
				case 2:
					dice1 = 2;
					break;
				case 3:
					dice1 = 3;
					break;
				case 4: 
					dice1 = 4;
					break;
				case 5:
					dice1 = 5;
					break;
				case 6:
					dice1 = 6;
					break;
				default:
					System.out.println("You choose a number outside of valid range");
					System.exit(0); ///ending program due to invalid input by user with output
					break;
			}
			
			System.out.println ("Please choose a value for the second die"); ///ask user for first die value between 1-6
			dice2 = myScanner.nextInt();
			switch ( dice2 ) {
				case 1:
					dice2 = 1;
					break;
				case 2:
					dice2 = 2;
					break;
				case 3:
					dice2 = 3;
					break;
				case 4: 
					dice2 = 4;
					break;
				case 5:
					dice2 = 5;
					break;
				case 6:
					dice2 = 6;
					break;
				default:
					System.out.println("You choose a number outside of valid range");
					System.exit(0); ///ending program due to invalid input by user with output
					break;
			}
			
 	}
	      switch ( dice1 ) {  ///switch statement to determine the roll of die one
					case 1:
						switch ( dice2 ) {  ///nested if statement to determin slang terminology based on die one roll and possible die two roll
							case 1: 
								dieRoll = "Snake Eyes";
								break;
							case 2: 
								dieRoll = "Ace Duece";
							  break;
							case 3:
								dieRoll = "Easy Four";
								break;
							case 4:
								dieRoll = "Fever Five";
								break;
							case 5: 
								dieRoll = "Easy Six";
								break;
							case 6:
								dieRoll = "Seven Out";
								break;
							default:
								break;
						} break;
					case 2:
						 switch ( dice2 ) {
							case 1: 
								dieRoll = "Ace Duece";
								break;
							case 2: 
								dieRoll = "Hard Four";
							  break;
							case 3:
								dieRoll = "Fever Five";
								break;
							case 4:
								dieRoll = "Easy Six";
								break;
							case 5: 
								dieRoll = "Seven Out";
								break;
							case 6:
								dieRoll = "Easy Eight";
								break;
							default:
								break;
						} break;
						case 3:
						 switch ( dice2 ) {
							case 1: 
								dieRoll = "Easy Four";
								break;
							case 2: 
								dieRoll = "Fever Five";
							  break;
							case 3:
								dieRoll = "Hard Six";
								break;
							case 4:
								dieRoll = "Seven Out";
								break;
							case 5: 
								dieRoll = "Easy Eight";
								break;
							case 6:
								dieRoll = "Nine";
								break;
							default:
								break;
						} break;
						case 4:
						 switch ( dice2 ) {
							case 1: 
								dieRoll = "Fever Five";
								break;
							case 2: 
								dieRoll = "Easy Six";
							  break;
							case 3:
								dieRoll = "Seven Out";
								break;
							case 4:
								dieRoll = "Hard Eight";
								break;
							case 5: 
								dieRoll = "Nine";
								break;
							case 6:
								dieRoll = "Easy Ten";
								break;
							default:
								break;
						} break;
						case 5:
						 switch ( dice2 ) {
							case 1: 
								dieRoll = "Easy Six";
								break;
							case 2: 
								dieRoll = "Seven Out";
							  break;
							case 3:
								dieRoll = "Easy Eight";
								break;
							case 4:
								dieRoll = "Nine";
								break;
							case 5: 
								dieRoll = "Hard Ten";
								break;
							case 6:
								dieRoll = "Yo-leven";
								break;
							default:
								break;
						} break;
						case 6:
						 switch ( dice2 ) {
							case 1: 
								dieRoll = "Seven Out";
								break;
							case 2: 
								dieRoll = "Easy Eight";
							  break;
							case 3:
								dieRoll = "Nine";
								break;
							case 4:
								dieRoll = "Easy Ten";
								break;
							case 5: 
								dieRoll = "Yo-leven";
								break;
							case 6:
								dieRoll = "Boxcars";
								break;
							default:
								break;
						} break;
					} ///end of switch
	
	System.out.println("You rolled a " + dieRoll + ", Thanks for playing"); ////outputs slang terminology
											
				}
}