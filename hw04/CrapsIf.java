///Conor Leahy   CSE002 
////HW 04 Part 1 CrapsIf.java
///Evaluate and output slang terminology for a craps game given randomly generated values or picked values

import java.util.*;
///imports every class that java has

public class CrapsIf {
	///main method required by every java program
	public static void main (String [] args) {
		
		Scanner myScanner = new Scanner (System.in);
		///initialize the scanner
		
		String dieRoll = ""; ///declare string variable 
		int dice1 = 0; ///declare variables for die rolls
		int dice2 = 0;
		
		System.out.println("Type 1 to randomly cast die or type 2 to state die values to evauluate:");
		int choice1 = myScanner.nextInt(); ///store the value of user input
		
		if (choice1 == 1) {
			dice1 = (int)(Math.random()*6) + 1;  //generate a random variable from 1-6
			dice2 = (int)(Math.random()*6) + 1;
			System.out.println(dice1 + " " + dice2);    ////FOR TESTING PURPOSES
			}
		
		
		 else if (choice1 == 2) {
			System.out.println("Please choose a value for the first dice between 1 and 6"); ///asks user for value of first die
			dice1 = myScanner.nextInt(); ///stores value of first die
			     if ( dice1 > 6 || dice1 < 0) {
						 System.out.println("Error, value not within valid range. PROGRAM END ");///ends program with error message if number outside 1-6 chosen
						 System.exit(0);
					 }
			System.out.println("Please choose a value for the second dice between 1 and 6");
			dice2 = myScanner.nextInt();
			       if ( dice2 > 6 || dice2 < 0) {
						 System.out.println("Error, value not within valid range. PROGRAM END ");///ends program with error message if number outside 1-6 chosen
						 System.exit(0);
							 
					 }
		}
			else  {
			System.out.println("Did not type 1 or 2, INVALID, PROGRAM END");
				System.exit(0);
			}
			
			if (dice1 == 1 && dice2 == 1 ) { ///Assign a slang term to the combination given in if statement
			dieRoll = "Snake Eyes";
			}
			if (( dice1 == 1 && dice2 == 2 ) || (dice1 == 2 && dice2 == 1)) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Ace Deuce";
			}
			if (( dice1 == 1 && dice2 == 3 ) || (dice1 == 3 && dice2 == 1 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Four";
			}
			if (( dice1 == 1 && dice2 == 4 ) || (dice1 == 4 && dice2 == 1 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Fever Five";
			}
		  if (( dice1 == 1 && dice2 == 5 ) || (dice1 == 5 && dice2 == 1 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Six";
			}
		  if (( dice1 == 1 && dice2 == 6 ) || (dice1 == 6 && dice2 == 1 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Seven Out";
			}
		  if (( dice1 == 2 && dice2 == 2 ) || (dice1 == 2 && dice2 == 2 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Hard Four";
			}
		  if (( dice1 == 2 && dice2 == 3 ) || (dice1 == 3 && dice2 == 2 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Fever Five";
			}
		  if (( dice1 == 2 && dice2 == 4 ) || (dice1 == 4 && dice2 == 2 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Six";
			}
			if (( dice1 == 2 && dice2 == 5 ) || (dice1 == 5 && dice2 == 2 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Seven Out";
			}
		  if (( dice1 == 2 && dice2 == 6 ) || (dice1 == 6 && dice2 == 2 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Eight";
			}
			if (( dice1 == 3 && dice2 == 3 ) || (dice1 == 3 && dice2 == 3 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Hard Six";
			}
			if (( dice1 == 3 && dice2 == 4 ) || (dice1 == 4 && dice2 == 3 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Seven Out";
			}
			if (( dice1 == 3 && dice2 == 5 ) || (dice1 == 5 && dice2 == 3 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Eight";
			}
			if (( dice1 == 3 && dice2 == 6 ) || (dice1 == 6 && dice2 == 3 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Nine";
			}
			if (( dice1 == 4 && dice2 == 4 ) || (dice1 == 4 && dice2 == 4)) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Hard Eight";
			}
			if (( dice1 == 4 && dice2 == 5 ) || (dice1 == 5 && dice2 == 4)) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Nine";
			}
			if (( dice1 == 4 && dice2 == 6 ) || (dice1 == 6 && dice2 == 4 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Easy Ten";
			}
			if (( dice1 == 5 && dice2 == 5 ) || (dice1 == 5 && dice2 == 5 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Hard Ten";
			}
			if (( dice1 == 5 && dice2 == 6 ) || (dice1 == 6 && dice2 == 5 )) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Yo-leven";
			}
			if ( dice1 == 6 && dice2 == 6 ) { ///make sure to factor in combination of both die when not same value
			dieRoll = "Boxcars";
			}
		System.out.println ("you rolled a " + dieRoll + ", Thanks for playing"); ///outputs slang terminology
		 
					
			
		
			
			
		}
		
		
		
		
		
	}
	
	
