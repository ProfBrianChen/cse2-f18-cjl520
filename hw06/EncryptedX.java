import java.util.*;
public class EncryptedX {
public static void main(String [] args) {

	Scanner Scan = new Scanner (System.in);  /// Set up instance of scanner and name Scan
	int input = 0;    /// declare variable for user input of the number of stars in each row
	boolean intcheck = false;  ///bolean used to end loop to test user input
	String temp = "";   ///temporary variable used to clear scanner
	int count = 1;  ///Counter used to set up loops for pattern of stars
	
		
	
	System.out.println("Enter an integer from 1-100: ");  //Asks user for input of integeter from 1-100
	while (intcheck == false) { ///makes sure the number entered is an integer from 1 to 100
		if (Scan.hasNextInt()) { //boolean test for whether input is an integer 
		     input = Scan.nextInt(); //input becomes user input
		  if (input <= 100 && input > 0) {
			  intcheck = true; //ends loop if both conditions of integer and value from 1-100 are met
			 
		  }
		  else {
			  System.out.println("Error: Please enter integer from 1-100: ");  ///Error message before loop repeats and user inputs new integer
		  }
		}
		else { 
		temp = Scan.next(); ///assigns user input to temp value if they input incorrect value  //clears scanner for new integer 
		System.out.println("Error: Please enter integer from 1-100: ");  
		}
	}
	
	for ( count = 1; count < input; count++) {   //sets up loop for pattern, repeat until equal to user input, 
		//meaning you will fill up required star number for each row
		System.out.println(""); ///starts new line after each row is printed
		for (int rowcount = 1; rowcount < input; rowcount++) {  ///set up loop so number of rows is equal to user input as well
			if (rowcount == count || rowcount == (input-count)) { ///sets up when spaces in pattern are and x pattern, 
				//two spaces for every row except middle one which would a have 1 
				System.out.print(" ");
			}
			else {
				System.out.print("*"); //fills all other spots in row with stars 
			}
		}
		
	}
 	
	
	
    }
}