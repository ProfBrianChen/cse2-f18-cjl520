///CSE002 Conor Leahy hw07
//

import java.util.*;  //import all objects including scanner object
public class WordTools {
	//make scanner static for use in all methods
	static Scanner Scan = new Scanner(System.in);
	
	///initiate main method
	public static void main(String[] args) {
		//variable declaration
		String menuChoice = " ";  ///sets up variable for menu choice input from user
		String phrase = " "; //saves phrase when user inputs something after hitting f in menu
		String text = " "; ///saves the sample text input from user in samSpleText method
			
		text = sampleText();
		 
		 

	
		while (true) { ///menu always reruns until quit so keep variable true
			//initial print statement of menu so user knows inputs  
			System.out.println ("MENU ");
			System.out.println ("");
			System.out.println ("c - Number of non-whitespace characters");
			System.out.println ("w - Number of words");
			System.out.println ("f - Find Text");
			System.out.println ("r - replace all !'s");
			System.out.println ("s - Shortn spaces");
			System.out.println ("q - Quit ");
			System.out.println ("");
			System.out.println ("Choose an Option");
			///want loop to always run because quit option should be only way out 
			///test to see if input is valid before running printMenu method
			//initial print statement of method so it appears  
			menuChoice = Scan.nextLine(); ///assigns menu choice with user input
			///test validity of user input for menu
			
			if (menuChoice.equalsIgnoreCase("c") || menuChoice.equalsIgnoreCase("w") || 
					menuChoice.equalsIgnoreCase("f") || menuChoice.equalsIgnoreCase("r") || 
					menuChoice.equalsIgnoreCase("s") || menuChoice.equalsIgnoreCase("q")) {
				
				//if they chose to scan for a phrase it declares a variable as the phrase
				if (menuChoice.equalsIgnoreCase("f")) {
					System.out.println("What phrase or word would you like to look for");
					phrase = Scan.nextLine();
				}	
				//pass the variables needed for printMenu Method (user input for menu, full user input text, desired phrase for "f" in menu)
			printMenu (menuChoice, text, phrase);
			continue; ///restarts loop if all for loop is true until now
			}
				//pass variables to the printMenu method 
				
				else { //make sure to have a default statement that asks user for 
					//valid choice if they above are false
					System.out.println("Enter a valid choice");
			}
			
	}
	}
	
	
	

	
	///method for prompting and having user enter a sample text
	public static String sampleText() {
		//declare variables
		String sample = "";
		
		///ask user for input
		System.out.println("Enter a phrase or sample text of your choosing");
		///Scanner picks up next line that user types
		sample = Scan.nextLine();
		///outputs what the user typed
		System.out.println("You entered " + sample);
	    
		//return the sample string value for use in main method
		return sample;
	}
	
	
	
	/// method for printing the menu options and validate user choices for menu\
	public static void printMenu (String a, String text, String phrase) {
		///string a is the user input, string text is the original sample that the user input,
		///string phrase is phrase user looks for if they choose "f - Find text"
	///set up switch statements so if correct value is entered the correct method is run for user
     switch (a) { 
     case "c":
    	 getNumofNonWSCharacters(text);  ///outputs number of non-whitespace characters
    	 break;
     case "w":
    	 getNumOfWords(text);   ///outputs number of words in text
    	 break;
     case "f": 
    	 findText(text, phrase);    ///allows user to see how many times a phrase appears in text
    	 break;
     case "r":
    	 System.out.println(replaceExclamation(text));   // replaces all exclamation marks in text to period
    	 //and returns this new text value to text in main method
    	 break;
     case "s":
    	 System.out.println(shortenSpace(text));  //replaces all spaces with single spaces and sends back to main method
    	 break;
     case "q":
    	 quit(); //quits program
    	 break;
     }
    
	System.out.println("Choose another Option");   ///prompts user for another menu input 
	System.out.println("Press 'q' to quit");  //gives user option to quit
	}		
	
//counts the number of non-whitespace characters in the sample text the user input
public static void getNumofNonWSCharacters (String text) {	
int countWS = 0; //initialize counter for white spaces within text
for (int i = 0; i < text.length(); i++ ) { ///number of loops is equal to the length of the text based on method
	if (text.charAt(i) == ' ') {// do nothing if a character is in that space	
	}
	else {
		countWS++;  //the counter for the white space goes up every time there is no char in a slot
	}
}
System.out.println("# of non-white space characters: " + countWS);
}


///set up method that counts the number of words by counting spaces between groups of text
///starts at one because no space after the last word
public static void getNumOfWords(String text) {
int countWords = 1; //set up counter for number of words
	for (int i = 0; i < text.length(); i++) {
	if (text.charAt(i) == ' ' && text.charAt(i + 1) != ' ') {  ///only adds to word counter if there 
		//is a space and the next space isnt empty or another space
		countWords++;
	}
	else {  ///set up default
}
}
System.out.println ("# of words: " + countWords);
}


public static void findText (String text, String phrase) {  ///uses full user text input and desired user phrase as inputs
	int countphrase = 0; //counts the number of phrases 

while (text.contains(phrase)) {  ///boolean outputs true if phrase is in text so loop repeats
	if (text.contains(phrase)) { 
	text = text.replaceFirst(phrase,  ""); ///replaces first instance of phrase in text with nothing
	countphrase++;  ///counter increases each loop through until phrase no longer in text
	}

}
System.out.println( phrase + " appears " + countphrase + " times" ); //outputs number of times phrase appears in text
}



//replaces all "!" with "."
public static String replaceExclamation (String text) {
	text = text.replaceAll("!",  "."); //replaces all exclamation points with perods
	return text;  ///returns the text to be used in main method later
}
///replaces all instances of two spaces together with single space
public static String shortenSpace (String text) {
	text = text.replaceAll("( )+", " ");  //replaces any amount of space with a single space
	return text; /// returns the text to be used in main method later
}


//exits the program 
public static void quit () {
	System.exit(0);
}


}
